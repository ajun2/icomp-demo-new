import loaderController from './loader.controller';

export const loaderComponent = {
	templateUrl	: `/src/common/loader/loader.html`,
    controller : loaderController,
    bindings: {
        loader: '<'
    }
};