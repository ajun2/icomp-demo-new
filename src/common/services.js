import angular from 'angular';

class apiServ
{
  constructor($http)
  {
    this.$http = $http;
    // this.test = test();
  }

  test(){
      console.log('test');
  }

  getConfig() {
    return { headers: { 'X-icomp-uuid': localStorage.ip, 'Authorization': localStorage.token, 'X-icomp-user-id': localStorage.user_id } };
  }

  httpPost(apiUrl, data){
    return this.$http.post(apiUrl, data).then(result => result.data);
  }

  httpPostHeader(apiUrl, data){
    return this.$http.post(apiUrl, data, this.getConfig()).then(result => result.data);
  }

  httpGet(apiUrl){
    return this.$http.get(apiUrl).then(result => result.data);
  }

  httpGetHeader(apiUrl){
    console.log('header');
    return this.$http.get(apiUrl, this.getConfig()).then(result => result.data);
  }
}

apiServ.$inject = ['$http'];

export default apiServ;