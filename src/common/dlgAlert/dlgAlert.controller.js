class dlgAlertController {
    constructor($scope) {
        this.$scope = $scope;

        this.$scope.$watch(() => this.title, nv => {
            if(nv == 'failed'){
                console.log('failed', nv);    
                $('#dlgAlert').modal('show');            
            } else {                
                console.log('success', nv);
            }
        });
    }
}

dlgAlertController.$inject = ['$scope'];
export default dlgAlertController;