import dlgAlertController from './dlgAlert.controller';

export const dlgAlertComponent = {
	templateUrl	: `/src/common/dlgAlert/dlgAlert.html`,
    controller : dlgAlertController,
    bindings: {
        message: '<',
        title: '<',
        url:'<'
    }
};