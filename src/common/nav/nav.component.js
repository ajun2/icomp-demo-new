import navController from './nav.controller';

export const navComponent = {
	templateUrl	: `/src/common/nav/nav.html`,
    controller : navController,
    bindings: {
        other: '<'
    }
};