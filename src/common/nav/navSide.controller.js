import objVal from '../variable.js';

class navSideController {
    constructor($rootScope, $location, $q, apiServ) {
        // this.$scope = $scope;
        this.$rootScope = $rootScope;
        this.$location = $location;
        this.$q = $q;
        this.role = localStorage.role;
        this.company_name = localStorage.company_name;
        this.name = localStorage.name;
        this.apiServ = apiServ;
        this.loginMenu = false;
    }

    $onInit() {
        this.$rootScope.$on('$routeChangeSuccess', () => {
            this.role = localStorage.role;
            this.company_name = localStorage.company_name;
            this.name = localStorage.name;
            if (this.getLocation()) {
                let promise = this.permissionCheck(objVal.permissionCheckObj);
                promise.then((response) => {
                    this.permissionCheckAfter(response);
                });
            }
        });
    }
    
    permissionCheck(url) {
        return this.$q((resolve, reject) => {
            this.apiServ.htttpGetHeader(url).then(
                response => resolve(response)
            );
        });        
    }

    permissionCheckAfter(response){
        if (response.status !== 'failed') {
            this.loginMenu = true;
        }
    }
}

navSideController.$inject = ['$rootScope', '$location', '$q', 'apiServ'];
export default navSideController;