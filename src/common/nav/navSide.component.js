import navSideController from './navSide.controller';

export const navSideComponent = {
	templateUrl	: `/src/common/nav/navSide.html`,
    controller : navSideController,
    bindings: {
        other: '<'
    }
};