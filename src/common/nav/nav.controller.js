import objVal from '../variable.js';

class navController {
    constructor($rootScope, $location, $q, apiServ) {
        // this.$scope = $scope;
        this.$rootScope = $rootScope;
        this.$location = $location;
        this.$q = $q;
        this.role = localStorage.role;
        this.company_name = localStorage.company_name;
        this.name = localStorage.name;
        this.apiServ = apiServ;
        this.loginMenu = false;
    }

    $onInit() {
        this.$rootScope.$on('$routeChangeSuccess', () => {
            this.role = localStorage.role;
            this.company_name = localStorage.company_name;
            this.name = localStorage.name;
            console.log(this.role);

            this.top_menu = this.getTopMenu();
            if (this.getLocation()) {

                let promise = this.permissionCheck(objVal.permissionCheckObj);
                promise.then((response) => {
                    this.permissionCheckAfter(response);
                });
            }
        });
    }
    
    permissionCheck(url) {
        return this.$q((resolve, reject) => {
            this.apiServ.htttpGetHeader(url).then(
                response => resolve(response)
            );
        });        
    }

    permissionCheckAfter(response){
        if (response.status !== 'failed') {
            if (response.roles !== undefined && response.roles !== null) {
                this.permission = response;
                this.permission.roles = JSON.stringify(this.permission.roles);
                if (this.$location.path() === '/') {
                    if (this.permission.roles.includes('General User')) {
                        this.$location.url('/asbestos/mySurvey');
                    } else if (this.permission.roles.includes('Company Admin')) {
                        this.$location.url('/asbestos/home');
                    }
                } else if (this.$location.path() === '/asbestos/home') {
                    if (this.permission.roles.includes('General User')) {
                        this.$location.url('/asbestos/mySurvey');
                    }
                }
            }
            this.loginMenu = true;
        }
    }

    getTopMenu() {
        return !!(this.$location.path().includes('/workplace_survey') || this.$location.path().includes('/iso27001') || this.$location.path().includes('/asbestos_survey'));
    }

    getLocation() {
        return this.$location.path() !== '/registration' && this.$location.path() !== '/login' && !this.$location.path().includes('/hex') && this.$location.path() !== '/new_password' && !this.$location.path().includes('/reset_password');
    }
}

navController.$inject = ['$rootScope', '$location', '$q', 'apiServ'];
export default navController;