export default function route($locationProvider, $routeProvider) {
  
    // $locationProvider.html5Mode(true);
    // $locationProvider.hashPrefix('');

    $routeProvider
    .when('/login', {template: '<app-login><app-login>', reloadOnSearch: true}) //Login page
    .when('/asbestos/home', {template: '<app-home><app-home>', reloadOnSearch: true}) //Home page
    .when('/admin/customer', {template: '<app-customer><app-customer>', reloadOnSearch: true}) //Customer page
    
    .otherwise({
        redirectTo: '/login'
    });
}

route.$inject = ['$locationProvider', '$routeProvider'];