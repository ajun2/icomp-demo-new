import objVal from '../common/variable.js';
import SHA512 from 'sha512-es';

class loginController {
    constructor($q, $http, $location, apiServ) {
        this.$q = $q;
        this.$http = $http;
        this.$location = $location;
        this.service = apiServ;     
        this.processing = false;
        this.loader = false;
        this.alertMessage = '';
        this.alertTitle = '';
        this.processing = false;
        this.loader = false;
    }

    $onInit() {
        // this.login('email', 'pw');
    }

    login(email, pw){
        pw = SHA512.hash(pw);        
        this.processing = true;
        this.loader = true;
        let data = '{"email":"' + email + '", "password":"' +pw + '"}';
        let promise = this.loginCall(objVal.loginObj, data);
        promise.then((response) => {
            this.loginAfter(response);
        });
    }

    loginCall(url, data){
        console.log('loginCall');
        return this.$q((resolve, reject) => {
            this.service.httpPost(url, data).then(
                response => resolve(response)
            );
        });
    }

    loginAfter(response){
        console.log('loginAfter');
        if (response.status !== undefined && response.status !== null) {
            this.processing = false;
            this.loader = false;
            if (response.status === 'failed') {
                this.alertMessage = response.message;
                this.alertTitle = response.status;
            } else {
                localStorage.setItem("ip", response.ip);
                localStorage.setItem("token", response.token);
                localStorage.setItem("user_id", response.user_id);
                localStorage.setItem("role", response.roles[0]);
                localStorage.setItem("company_name", response.company_name);
                localStorage.setItem("name", response.firstname);
                
                if (response.company_name === 'ISO27001') {
                    this.$location.path('/iso27001/survey');
                } else if (response.roles[0] === 'Super Admin') {
                    this.$location.path('/admin/customer');
                } else if (response.roles[0] === 'Company Admin') {
                    this.$location.path('/asbestos/home');
                } else {
                    this.$location.path('/asbestos/mySurvey');
                }
                // this.$parent.role = response.roles[0];
            }
        }
    }
}

loginController.$inject = ['$q', '$http', '$location', 'apiServ'];
export default loginController;