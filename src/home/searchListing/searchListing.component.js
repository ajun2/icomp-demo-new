import searchListingController from './searchListing.controller';

export const searchListingComponent = {
	templateUrl	: `/src/home/searchListing/searchListing.html`,
    controller : searchListingController,
    bindings: {
        search: '=',
    },
};