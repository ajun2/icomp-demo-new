import homeController from './home.controller';

export const homeComponent = {
	templateUrl	: `/src/home/home.html`,
    controller : homeController,
    // bindings: {
    //     connection: '='
    // },
};