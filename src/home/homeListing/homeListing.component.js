import homeListingController from './homeListing.controller';

export const homeListingComponent = {
	templateUrl	: `/src/home/homeListing/homeListing.html`,
    controller : homeListingController,
    bindings: {
        search: '=',
        loader:'='
    },
};