import objVal from '../../common/variable.js';

class homeListingController {
    constructor(apiServ, $location, $q) {
        this.workplaces = {};
        this.service = apiServ;
        this.$location = $location;
        this.$q = $q;
    }

    $onInit() {
        this.getHomeListing();
    }

    getHomeListing(){
        console.log('getHomeListing');
        this.loader = true;
        const url = objVal.assingListObj + '?self=admin';
        let promise = this.getHome(url);
        promise.then((response) => {
            this.getHomeAfter(response);
        });
    }

    getHome(url, data){
        console.log('getHome');
        return this.$q((resolve, reject) => {
            this.service.httpGetHeader(url).then(
                response => resolve(response)
            );
        });
    }

    getHomeAfter(response){
        console.log('getHomeAfter');
        if (response.status !== undefined && response.status !== null) {
            if (response.status === 'success') {
                this.workplaces = response.data;
            } else {
                this.$location.path('/login');
            }
        }
        this.loader = false;
        // this.alertMessage = response.message;
        // this.alertTitle = response.status;
    }
}
homeListingController.$inject = ['apiServ', '$location', '$q'];
export default homeListingController;