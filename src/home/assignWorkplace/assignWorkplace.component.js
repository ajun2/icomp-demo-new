import assignWorkplaceController from './assignWorkplace.controller';

export const assignWorkplaceComponent = {
	templateUrl	: `/src/home/assignWorkplace/assignWorkplace.html`,
    controller : assignWorkplaceController,
    bindings: {
        loader: '=',
        message: '=',
        title: '=',
        url:'='
    },
};