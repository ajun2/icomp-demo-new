import objVal from '../../common/variable.js';
import 'angular-moment-picker/dist/angular-moment-picker.min.css';

class assignWorkplaceController {
    constructor($q, apiServ) {
        this.$q = $q;
        this.service = apiServ;
        console.log('assign workplace test3');
    }

    $onInit() {
        console.log('assign workplace test2');
    }

    submitAssigned(postData){
        // httpPostHeader
        this.loader = true;
        let promise = this.submitCall(objVal.assingObj, postData);
        promise.then((response) => {
            this.submitAfter(response);
        });

    }

    submitCall(url, data){
        console.log('submitCall');
        return this.$q((resolve, reject) => {
            this.service.httpPostHeader(url, data).then(
                response => resolve(response)
            );
        });
    }

    submitAfter(response){
        console.log('submitAfter');
        this.assignSurvey = {};
        // this.assing_survey.$setPristine();
        // this.assing_survey.$setUntouched();
        if (response.status !== undefined && response.status !== null) {
            // if (response.status === 'success') {
            //     this.listAssigned();
            // }
            this.assigning = false;
        }
        this.loader = false;
        this.alertMessage = response.message;
        this.alertTitle = response.status;
    }
}
assignWorkplaceController.$inject = ['$q', 'apiServ'];
export default assignWorkplaceController;