import 'bootstrap';
import 'bootstrap/scss/bootstrap.scss';
import './css/style.scss';

import angular from 'angular';
import ngRoute from 'angular-route';
import 'angular-moment-picker';
import routing from './app.config';

import {dlgAlertComponent} from './common/dlgAlert/dlgAlert.component';
import {loaderComponent} from './common/loader/loader.component';
import {navComponent} from './common/nav/nav.component';
import {navSideComponent} from './common/nav/navSide.component';
import apiServ from './common/services';

import {loginComponent} from './login/login.component';
import {homeComponent} from './home/home.component';
import {assignWorkplaceComponent} from './home/assignWorkplace/assignWorkplace.component';
import {searchListingComponent} from './home/searchListing/searchListing.component';
import {homeListingComponent} from './home/homeListing/homeListing.component';
import {customerComponent} from './customer/customer.component';

angular.module('app', [ngRoute, 'moment-picker'])
    .component('appLogin', loginComponent)
    .component('appLoader', loaderComponent)
    .component('appNav', navComponent)
    .component('appNavSide', navSideComponent)

    .component('appHome', homeComponent)
    .component('appAssignWorkplace', assignWorkplaceComponent)
    .component('appSearchListing', searchListingComponent)
    .component('appHomeListing', homeListingComponent)

    .component('appCustomer', customerComponent)
    .component('dlgAlert', dlgAlertComponent)
    .service('apiServ', apiServ)
    .config(routing);