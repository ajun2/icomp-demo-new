import customerController from './customer.controller';

export const customerComponent = {
	templateUrl	: `/src/customer/customer.html`,
    controller : customerController,
    bindings: {
        other: '<'
    }
};